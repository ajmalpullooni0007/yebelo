import 'dart:convert';

import 'package:clay_containers/widgets/clay_container.dart';
import 'package:clay_containers/widgets/clay_text.dart';
import 'package:flutter/material.dart';
import 'package:yebelo/model.dart';
import 'package:flutter/services.dart' as rootBundle;
class AllProducts extends StatefulWidget {


  @override
  _AllProductsState createState() => _AllProductsState();
}

class _AllProductsState extends State<AllProducts> {
  List<int> qty = [0,0,0,0];

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFf2f2f2),
      child: FutureBuilder(
        future: ReadJsonData(),
        builder: (context, data) {
          if (data.hasError) {
            return Center(child: Text("${data.error}"));
          } else if (data.hasData) {
            var items = data.data as List<ProductList>;
            return ListView.builder(
                itemCount: items == null ? 0 : items.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 15.0),
                    child: Container(
                      color: Color(0xFFf2f2f2),
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: ClayContainer(
                            color: Color(0xFFf2f2f2),
                            width: double.infinity,
                            borderRadius: 50,
                            child: Row(
                              children: [
                                Expanded(
                                    flex: 1,
                                    child: Container(
                                        height: 80,
                                        child: Image.asset(items[index].pImage.toString(),fit: BoxFit.contain,))),
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 20),
                                        child: Column(
                                          //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Text(items[index].pName.toString(),style: TextStyle(fontSize: 20,fontWeight: FontWeight.w500),),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            if(items[index].pDetails.toString() != 'null')
                                            Text(items[index].pDetails.toString()),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Text("Availability : ${items[index].pAvailability.toString()}"),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Text("Cost : ${items[index].pCost.toString()}"),
                                            SizedBox(
                                              height: 7,
                                            ),
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Text("Quantity :    "),
                                                InkWell(
                                                  onTap: (){
                                                    if(qty[index] != 0){
                                                      setState(() {
                                                        qty[index]--;
                                                      });
                                                    }
                                                  },
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      color: Colors.grey,
                                                      shape: BoxShape.circle
                                                    ),
                                                    child: Icon(Icons.remove,color: Colors.black,size: 20,),
                                                  ),
                                                ),
                                                Text(qty[index].toString()),
                                                InkWell(
                                                  onTap: (){
                                                    setState(() {
                                                      qty[index]++;
                                                    });
                                                  },
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                        color: Colors.grey,
                                                        shape: BoxShape.circle
                                                    ),
                                                    child: Icon(Icons.add,color: Colors.black,size: 20,),
                                                  ),
                                                ),
                                              ],
                                            )

                                          ],
                                        ),
                                      )
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                });
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
    }









  Future<List<ProductList>> ReadJsonData() async {
    final jsondata = await rootBundle.rootBundle.loadString('assets/productlist.json');
    final list = json.decode(jsondata) as List<dynamic>;
    return list.map((e) => ProductList.fromJson(e)).toList();
  }
}
